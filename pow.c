#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* The POW (power) fucntion using two doubles. */
int main( void ) 
{

   /* User input. */
   double value1, value2;

   /* Stores the result of value1 to the power of value2. */
   double result;

   /* Get user input. */
   printf( "This program demonstrates the POW (power) functiion using two doubles.\n" );
   printf( "Pleae enter two numbers; the base number followed by the exponenent:\n" );
   scanf( "%lf%lf", &value1, &value2 );
   printf( "\n" );

   /* Calculates the power. */
   result = pow( value1, value2 );

   /* Displays the results. */
   printf( "%f raised to the power of %f is %f\n", value1, value2, result);
      
   return EXIT_SUCCESS; /* end main function */

}
